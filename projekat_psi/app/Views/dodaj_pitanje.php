<div class="row">
            <div class="col-12 col-md-6 offset-md-3 text-center">
                <h3>Postavite pitanje</h3>
                <form action="http://localhost:8080/Pitanja/postavi" method="post">
                    <div class="mb-3">
                      <label for="naslov" class="form-label">Naslov</label>
                      <input type="text" class="form-control" id="naslov" name="naslov">
                    </div>
                      <div class="form-floating">
                        <textarea class="form-control" id="pitanje" style="height: 100px" name="poruka"></textarea>
                        <label for="poruka">Pitanje</label>
                      </div>
                    <button type="submit" class="btn btn-success mt-3" id="postavi_pitanje" name="pitanje">Postavite pitanje</button>
                  </form>
            </div>
        </div>