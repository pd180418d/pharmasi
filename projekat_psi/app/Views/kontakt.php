<div class="row">
            <div class="col-12 col-md-6 offset-md-3 text-center">
                <h2>Kontaktirajte nas</h2>
                <hr>
                <form action="http://localhost:8080/Kontakt/slanje" method="post">
                    <div class="mb-3">
                      <label for="naslov" class="form-label">Naslov</label>
                      <input type="text" class="form-control" id="naslov" name="naslov">
                    </div>
                    <div class="mb-3">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" class="form-control" id="email" aria-describedby="emailHelp" name="email">
                    
                    </div>
                      <div class="form-floating">
                        <textarea class="form-control" id="poruka" style="height: 100px" name="poruka"></textarea>
                        <label for="poruka">Poruka</label>
                      </div>
                    <button type="submit" class="btn btn-success mt-3" id="posalji_poruku">Posaljite nam poruku</button>
                  </form>
            </div>
    
            <div class="row mt-5" id="kontakt_str">
            <div class="col-12 col-md-3">
              <i class="fas fa-envelope-square"></i>
                samoizolacija@pharmasi.ae
            </div>
            <div class="col-12 col-md-6">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d462562.85275386384!2d54.94687367874806!3d25.075706034904837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f43496ad9c645%3A0xbde66e5084295162!2z0JTRg9Cx0LDQuCAtINCj0ZjQtdC00LjRmtC10L3QuCDQkNGA0LDQv9GB0LrQuCDQldC80LjRgNCw0YLQuA!5e0!3m2!1ssr!2srs!4v1616177962044!5m2!1ssr!2srs" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
            <div class="col-12 col-md-3">
              <i class="fas fa-phone"></i>
                0097143641344
            </div>
        </div>

    </div>
        </div>