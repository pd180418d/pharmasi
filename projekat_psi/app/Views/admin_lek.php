<div class="row">
            <div class="col-12 text-center">
                <h3 id="odg" style="background-color: rgba(255, 0, 0, 0.431); ">
                    <?php 
                        if(isset($odgovor)){
                            echo $odgovor;
                        }
                    ?>
                </h3>
            </div>
        </div>

<div class="row" id="lekovi">
            <div class="col-12 col-md-6 offset-md-3 text-center">
                <h1 class="mt-5">Lekovi</h1>
                <hr>
                <h3>Dodavanje lekova</h3>
                <form action="http://127.0.0.1/projekat_psi/public/admin/operacije" method="post">
                    <div class="mb-3">
                      <label for="naziv" class="form-label">Naziv</label>
                      <input type="text" class="form-control" id="naziv" name="naziv">
                    </div>
                    <div class="mb-3">
                      <label for="opis" class="form-label">opis</label>
                      <input type="text" class="form-control" id="opis" name="opis">
                    </div>
                    <div class="mb-3">
                      <label for="uputstvo" class="form-label">uputstvo</label>
                      <input type="text" class="form-control" id="uputstvo" name="uputstvo">
                    </div>
                    <div class="mb-3">
                        <label for="cena" class="form-label">Cena</label>
                        <input type="number" class="form-control" id="cena" name="cena" min="1">
                      </div>
                    <button type="submit" class="btn btn-success" name="dodaj_lek">Dodaj</button>
                  </form>
    </div>
</div>

<div class="row">
    <?php 
        $lekovi = $podaci;
        foreach($lekovi as $lek){
            ?>
                <div class="col-12 col-md-4 offset-md-4 text-center">
                <form action="http://localhost:8080/admin/izmeni_lek" method="post" class="mt-5">
                    <input type="hidden" name="id" value="<?php echo $lek->idLek; ?>">
                    <div class="card text-center">
                        <div class="card-header">
                          Cena: <input type="number" name="cena" value="<?php echo $lek->Cena; ?>" min="1">
                        </div>
                        <div class="card-body">
                          <h5 class="card-title">Naziv: <input type="text" name="naziv" value="<?php echo $lek->Naziv; ?>"></h5>
                          
                          <button type="submit" class="btn btn-success" name="izmeni" value="izmeni">Izmeni</button>
                          <button type="submit" class="btn btn-danger" name="izmeni" value="obrisi">Obrisi</button>
                        </div>
                        
                      </div>
                </form>
                </div>
            <?php
        }
    ?>
</div>