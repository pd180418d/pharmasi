<div class="row">
            <div class="col-12 text-center">
                <h3 id="odg" style="background-color: rgba(255, 0, 0, 0.431); ">
                    <?php 
                        if(isset($odgovor)){
                            echo $odgovor;
                        }
                    ?>
                </h3>
            </div>
        </div>


        <div class="row">
            <div class="col-12 col-md-4 offset-md-4 text-center">
                <h2>Promenite informacije</h2>
                <hr>
                <form action="http://localhost:8080/profil/change" method="post">
                    <div class="mb-3">
                        <label for="sta" class="form-label">Status</label>
                        <input type="text" class="form-control" id="sta" disabled value="<?php
                        if($podaci[0]->Status==0){
                            echo "nije potvrdjeno";
                        }else{
                            echo "potvrdjeno";
                        }
                         ?>">
                      </div>
                    <div class="mb-3">
                        <label for="ime" class="form-label">Ime</label>
                        <input type="text" class="form-control" id="ime" name="ime" disabled value="<?php echo $podaci[0]->Ime; ?>">
                      </div>
                      <div class="mb-3">
                        <label for="prezime" class="form-label">Prezime</label>
                        <input type="text" class="form-control" id="prezime" name="prezime" disabled value="<?php echo $podaci[0]->Prezime; ?>">
                      </div>
                    <div class="mb-3">
                      <label for="korime" class="form-label">Korisnicko ime</label>
                      <input type="text" class="form-control" id="korime" name="korime" disabled value="<?php echo $podaci[0]->username; ?>">
                    </div>
                    <div class="mb-3">
                        <label for="lozinka" class="form-label">Lozinka</label>
                        <input type="password" class="form-control" id="lozinka" name="lozinka" value="<?php echo $podaci[0]->password; ?>">
                      </div>
                      <div class="mb-3">
                        <label for="potvrda" class="form-label">Potvrda lozinke</label>
                        <input type="password" class="form-control" id="potvrda" name="potvrda">
                      </div>
                      <div class="mb-3">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" class="form-control" id="email" aria-describedby="emailHelp" disabled value="<?php echo $podaci[0]->email; ?>">
               
                      </div>
                    <button type="submit" class="btn btn-success" id="promeni">Promeni</button>
                  </form>
            </div>
        </div>
        <form action="http://localhost:8080/Logout" method=post>
        <button type="submit" class="btn btn-danger" id="izloguj">Izloguj se</button>
        </form>
       
    </div>