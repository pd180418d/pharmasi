<div class="row" id="korisnici">
    <div class="col-12 col-md-6 offset-md-3 text-center">
        <h1 class="mt-5">Odobreni korisnici</h1>

        <?php
        $count = 1;
        foreach ($korisnici as $korisnik)
        {
        ?>
        
        <ul class="list-group" id="11">
            <li class="list-group-item">Id: <b><?php echo $count; ?></b></li>
            <li class="list-group-item">Ime: <b><?php echo $korisnik->username; ?></b></li>
            <li class="list-group-item">Prezime: <b><?php echo $korisnik->Prezime; ?></b></li>
            <li class="list-group-item">Korisnicko ime: <b><?php echo $korisnik->username; ?></b></li>
            <li class="list-group-item">Email: <b><?php echo $korisnik->email; ?></b></li>
            <li class="list-group-item">
               <?php
                if($korisnik->Status != 2)
                {
               ?> 
                <form action="<?php echo site_url("Admin/suspendujObrisi"); ?>" method="">
                   
                          <input type="hidden" name="username" value="<?php echo $korisnik->username; ?>">
                          <button type="submit" class="btn btn-warning" name="suspenduj" value="suspenduj" id="suspenduj" ">Suspenduj nalog</button>
                          <button type="submit" class="btn btn-danger" name="obrisi" value="obrisi" id="obrisi">Obrisi nalog</button>
                </form>  
               <?php
                }
                else
                {
               ?>
                <form action="<?php echo site_url("Admin/ukloniSuspenziju"); ?>" method="">
                          <input type="hidden" name="username" value="<?php echo $korisnik->username; ?>">
                          <button type="submit" class="btn btn-warning" 
                                  name="ukloniSuspenziju" value="ukloniSuspenziju" 
                                  id="ukloniSuspenziju">Ukloni suspenziju</button>
                </form>
               <?php
                }
               ?>     
                
            </li>
        </ul>
        
        <?php
        $count++;
        }
        ?>
        
    </div>
</div>