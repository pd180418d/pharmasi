<div class="row">
            <div class="col-12 text-center">
                <h3 id="odg" style="background-color: rgba(255, 0, 0, 0.431); ">
                    <?php 
                        if(isset($odgovor)){
                            echo $odgovor;
                        }
                    ?>
                </h3>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-md-4 offset-md-4 text-center">
                <h2>Registrujte se</h2>
                <hr>
                <form action="http://127.0.0.1/projekat_psi/public/registracija/data" method="post">
                    <div class="mb-3">
                        <label for="ime" class="form-label">Ime</label>
                        <input type="text" class="form-control" id="ime" name="ime" >
                      </div>
                      <div class="mb-3">
                        <label for="prezime" class="form-label">Prezime</label>
                        <input type="text" class="form-control" id="prezime" name="prezime" >
                      </div>
                    <div class="mb-3">
                      <label for="korime" class="form-label">Korisnicko ime</label>
                      <input type="text" class="form-control" id="korime" name="korime" >
                    </div>
                    <div class="mb-3">
                        <label for="lozinka" class="form-label">Lozinka</label>
                        <input type="password" class="form-control" id="lozinka" name="lozinka" >
                      </div>
                      <div class="mb-3">
                        <label for="potvrda" class="form-label">Potvrda lozinke</label>
                        <input type="password" class="form-control" id="potvrda" name="potvrda" >
                      </div>
                      <div class="mb-3">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp">
                      
                      </div>
                    <button type="submit" class="btn btn-success" id="registruj">Registruj se</button>
                  </form>
            </div>
        </div>