<?php

namespace App\Models;

use CodeIgniter\Model;

class AdminModel extends Model
{
    protected $table      = 'korisnik';
    protected $primaryKey = 'username';
    protected $returnType     = 'object';
    protected $allowedFields = ["Status"];
    
    public function zahtevi()
    {
        return $this->where("Status", 0)->findAll();
    }
    
    public function odobreni()
    {
        return $this->where("Status", 1)->orWhere("Status", 2)->findAll();
    }
    
    public function izmeni($username, $value)
    {
        $this->update($username, ["Status" => $value]);
    }  
    
    public function obrisi($username)
    {
        $this->delete($username);
    }
}