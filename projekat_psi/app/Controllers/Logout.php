<?php 

namespace App\Controllers;
   
use CodeIgniter\Controller;
/**
 * Logout - klasa za logout ulogovanih korisnika
 * Anja Mladenovic 2018/0478
 * @version 1.0
 */

class Logout extends BaseController{
    /**
     * Function _remap overriduje defaultne pozive u url-u i direktno poziva sebe
     * @return void
     * Unistava se trenutna sesija korisnika
     */
    public function _remap(){
       
        session_unset();
        session_destroy();

        echo view("head.php");
        echo view("navigacija.php");
        echo view("uloguj.php");
        echo view("footer.php");
    }
}

?>