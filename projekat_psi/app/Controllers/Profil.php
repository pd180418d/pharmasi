<?php 

namespace App\Controllers;
   
use CodeIgniter\Controller;
/**
 * Profil - klasa za prikaz svih podataka vezanih sa korisnika
 * Anja Mladenovic 2018/0478
 * @version 1.0
 */
class Profil extends BaseController{
/**
 * Funckija za promenu lozinke 
 */
  public function change(){
   
    echo view("head.php");
    echo view("navigacija.php");
    /**
         * isset function->@return bool
         * var array $k
         */
    
    if(isset($_SESSION["korisnik"])){
        $k = $_SESSION["korisnik"];

        /**
         * var string $lozinka password
         * var string $potvrda 
         */
        $lozinka = $_POST["lozinka"];
        $potvrda = $_POST["potvrda"];
        $db = \Config\Database::connect();

        /** 
         * Provera da li se nova lozinka i potvrda podudaraju
         */

        if($lozinka!=$potvrda){
            echo view("profil.php",["podaci"=>$k,"odgovor"=>"lozinka i potvrda se ne podudaraju"]);
        }else{
            /**
             * var string $sql
             * Izvrsava se upit nad bazom i upisuje se nova lozinka
             */
            $sql = "UPDATE korisnik SET password='".$lozinka."' WHERE username='".$k[0]->username."'";
            $db->query($sql);
            if($db->affectedRows()>0){
                /**
                 * Menja se lozinka u trenutnoj sesiji
                 */
                $_SESSION["korisnik"][0]->password=$lozinka;
                echo view("profil.php",["podaci"=>$_SESSION["korisnik"],"odgovor"=>"promenjeno"]);
            }else{
                echo view("profil.php",["podaci"=>$k,"odgovor"=>"greska"]);
            }
        }
    }else{
        echo view("uloguj.php",["odgovor"=>"ulogujte se"]);
    }

    
    
    echo view("footer.php");
  }
/**
 * Funkcija index prikazuje pocetnu stranu kontrolera za Profil
 */
    public function index(){
        
        echo view("head.php");
        echo view("navigacija.php");
        
        if(isset($_SESSION["korisnik"])){
            $k = $_SESSION["korisnik"];
            echo view("profil.php",["podaci"=>$k]);
        }else{
            echo view("uloguj.php",["odgovor"=>"ulogujte se"]);
        }

        
        
        echo view("footer.php");
    }

}

?>