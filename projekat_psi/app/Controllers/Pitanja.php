<?php 

namespace App\Controllers;
   
use CodeIgniter\Controller;
/**
 * Klasa Pitanja predstavlja postavljanje pitanja i prikazivanje stranice Pitanja
 * Isidora Milosevic2018/0660
 * @version 1.0
 */

class Pitanja extends BaseController{

    
  
/**
 * Funkcija postavi() ubacuje pitanja u bazu podataka tabelu pitanja i podrazumevano postavlja da na njih nije bilo odgovora
 * @return void
 */
    public function postavi(){
        /**
         * var string $poruka
         * var string $naslov
         * var datetime $datum
         */
            $poruka = $_POST["poruka"];
            $naslov = $_POST["naslov"];
            $datum = date("Y-m-d H:i:s");
            
            /**
             * var string $sql
             */
            
            $db = \Config\Database::connect();
            $sql = "INSERT INTO forma VALUES(NULL,'".$naslov."','".$poruka."','".$datum."')";
            $db->query($sql);
            /**
             * var int $ins_id
             * var string $username
             */
                
            $sql = "SELECT idForma FROM forma ORDER BY idForma DESC LIMIT 1";
            $query = $db->query($sql);
            $result = $query->getResult();
            $ins_id = $result[0]->idForma;
    
            $username = $_SESSION["korisnik"][0]->username;
    
            $sql = "INSERT INTO pitanje VALUES(".$ins_id.",'".$username."',0)";
            $db->query($sql);
            return redirect()->to("http://localhost:8080/Pitanja");
    }
/**
 * Funkcija index() predstavlja pocetnu stranicu stranice Pitanja
 * @return void
 */
    public function index(){
        echo view("head.php");
        echo view("navigacija.php");
        echo view("dodaj_pitanje.php");
        echo view("footer.php");
    }
}

?>