<?php 

namespace App\Controllers;
   
use CodeIgniter\Controller;
/**
 * Registracija - klasa za registrovanje novih korisnika
 * Anja Mladenovic 2018/0478
 * @version 1.0
 */

class Registracija extends BaseController{

    /**
     * Ucitavanje pocetne strane za prikaz stranice za Registraciju korisnika
     * @return void
     */
    public function index(){
        /**
         * isset function->@return bool
         * var array $k
         */
       
        if(isset($_SESSION['korisnik'])){
            $k = $_SESSION["korisnik"];
            echo view("head.php");
            echo view("navigacija.php");
            echo view("profil.php",["podaci"=>$k]);
        }else{
            echo view("head");
            echo view("navigacija");
            echo view("Registruj");
            echo view("footer");
        }
        
    }

    /**
     * @return void
     * Funkcija za prikupljanj,proveru i registrovanje podataka u okviru tabele korisnik 
     */


    public function data(){
        echo view("head.php");
        echo view("navigacija.php");
        /**
         * var string $ime Ime
         * var string $prezime Prezime
         * var string $korime  username
         * var string $lozinka password
         * var string $potvrda
         * var string $email email
         */

        $ime = $_POST["ime"];
        $prezime = $_POST["prezime"];
        $korime = $_POST["korime"];
        $lozinka = $_POST["lozinka"];
        $potvrda = $_POST["potvrda"];
        $email = $_POST["email"];
        /**
         * Provera da li su unete informacije u polja,u suprotnom se prosledjuje greska Views-Registruj.php
         */

        if(empty($ime) || empty($prezime) || empty($korime) || empty($lozinka) || empty($potvrda) || empty($email)){
            /**
             * var array $odg
             * var bool $v
             */
            $odg = ["odgovor"=>"svi podaci moraju biti uneseni"];
            $v=false;
        }
        /**
         * Provera da li se podudaraju lozinke,ako se ne podudaraju prosledjuje se greska Views-Registruj.php
         */

        if($lozinka!=$potvrda){
            $odg = ["odgovor"=>"lozinka i potvrda se ne poklapaju"];
            $v=false;
        }

        /**
         * var string $query
         */
        $v = true;

        $db = \Config\Database::connect();
        $query = $db->query("SELECT * FROM korisnik WHERE username='".$korime."'");
        $result = $query->getResult();
        /**
         * Proverava se da li je zauzeto korisnicko ime
         */
        if(count($result)>0){
            $odg = ["odgovor"=>"korisnicko ime zauzeto"];
            $v=false;
        }

        
        $query = $db->query("SELECT * FROM korisnik WHERE email='".$email."'");
        $result = $query->getResult();
        /**
         * Proverava se da li je email zauzet
         */
        if(count($result)>0){
            $odg = ["odgovor"=>"email je zauzet"];
            $v = false;
        }
        /**
         * Ako nije doslo do neke greske ubacuje se u tabelu 
         */

        if($v==true){
            $sql = "INSERT INTO korisnik VALUES (".$db->escape($korime).", ".$db->escape($lozinka).", ".$db->escape($email).", ".$db->escape($ime).", ".$db->escape($prezime).",0,0)";
            $db->query($sql);
            if($db->affectedRows()>0){
                $odg = ["odgovor"=>"registracija uspesna"];
            }else{
                $odg = ["odgovor"=>"greska"];
            }
        }
        
        
        echo view("Registruj.php",$odg);
        echo view("footer.php");
    }

}

?>