<?php 

namespace App\Controllers;
   
use CodeIgniter\Controller;
use App\Models\AdminModel;
/**
 * Klasa - Pocetna sluzi za prikazivanje svih lekova iz baze,za prikazivanje opcija filtera i takodje za informacije o svakom pojedinacnom leku
 * Dimitrije Popovic 2018/0418 Nemanja Ciric 2018/0548 Isidora Milosevic 2018/0660
 * @version 1.0
 */

class Admin extends BaseController{
    /**
     * Funkcija prikazuje sva pitanaj zajedno sa postojecim odgovorima zajedno sa svim detaljima (naslov,tekst,korisnik) kao i omogucava dodavanje novog odgovora
     * @return void
     */
    public function odgovori(){

        if(isset($_POST["izmeni"])){
            /**
             * var int $id
             * var string $odgovor
             * var string $sqlvar int $id
             */
            $id = $_POST["izmeni"];
            $odgovor = $_POST["odgovor"];

            $db = \Config\Database::connect();

            $sql = "SELECT forma.idForma AS id FROM pitanje,forma,odgovor WHERE odgovor.idPitanje=pitanje.idForma AND forma.idForma=odgovor.idForma AND pitanje.idForma=".$id;
            $query = $db->query($sql);
           
            $res = $query->getResult();
            $id = $res[0]->id;
         

            $sql = "UPDATE forma SET poruka='".$odgovor."' WHERE forma.idForma=".$id;
            $db->query($sql);
            return redirect()->to("http://localhost:8080/admin");
        }
        if(isset($_POST["odgovor"])){
        $odgovori = $_POST["odgovor"];
            $poruka = $_POST["poruka"];
            $naslov = "";
            $datum = date("Y-m-d H:i:s");
            
            
            $db = \Config\Database::connect();
            $sql = "INSERT INTO forma VALUES(NULL,'".$naslov."','".$poruka."','".$datum."')";
            $db->query($sql);
                
            $sql = "SELECT idForma FROM forma ORDER BY idForma DESC LIMIT 1";
            $query = $db->query($sql);
            $result = $query->getResult();
            $ins_id = $result[0]->idForma;
    
            $username = $_SESSION["korisnik"][0]->username;
    
            $sql = "INSERT INTO odgovor VALUES(".$ins_id.",'".$odgovori."','".$username."')";
            $db->query($sql);
            $db->query("UPDATE pitanje SET status=1 WHERE idForma=".$odgovori);
            
            return redirect()->to("http://localhost:8080/admin/Pitanja");
    }
    }
/**Funkcija vraca id forme koja je dodata poslednja za izmenu odgovora
 * @return int
 */
    function vrati_id_odgovora($pitanja){
        $niz = [];
        foreach($pitanja as $pitanje){
            $db = \Config\Database::connect();

            

            $sql = "SELECT forma.idForma FROM forma,pitanje,odgovor WHERE odgovor.idPitanje=pitanje.idForma AND forma.idForma=odgovor.idpitanje AND pitanje.idForma=".$pitanje->idforma;
            $query = $db->query($sql);
            $result = $query->getResult();
            //$a=new array($pitanje->idforma=>$pitanje->poruka);
            //array_push($niz,$a);
            //$niz[$pitanje->idforma][]=$pitanje->poruka;
            
            try{
            $niz[$pitanje->idforma][]=$result[0]->idForma;
            }catch(\Exception $e){}

            //var_dump($pitanje->idforma." je ".$niz[$pitanje->idforma]);
            
        }

        return $niz;
    }
/**Funkcija vraca odgovor na pitanje
 * @return string
 */
    function vrati_odgovore($pitanja){
        $niz = [];
        foreach($pitanja as $pitanje){
            $db = \Config\Database::connect();
            $sql = "SELECT forma.Poruka FROM pitanje,forma,odgovor WHERE odgovor.idPitanje=pitanje.idForma AND forma.idForma=odgovor.idForma AND pitanje.idForma=".$pitanje->idforma;
            $query = $db->query($sql);
            $result = $query->getResult();
            
            //$a=new array($pitanje->idforma=>$pitanje->poruka);
            //array_push($niz,$a);
            //$niz[$pitanje->idforma][]=$pitanje->poruka;
            try{
                $niz[$pitanje->idforma][]=$result[0]->Poruka;
            }catch(\Exception $e){}

            //var_dump($pitanje->idforma." je ".$niz[$pitanje->idforma]);
            
        }

        return $niz;
    }
/**Funkcija vraca sva postojeca pitanja
 * @return array
 * 
 */
    public function vrati_pitanja(){
        $db = \Config\Database::connect();
        $sql = "SELECT forma.idforma,forma.naslov,forma.poruka,forma.datumvreme,pitanje.username,pitanje.status FROM forma,pitanje WHERE forma.idForma=pitanje.idForma";
        $query = $db->query($sql);
        $result = $query->getResult();

        return $result;
    }
/*
    public function dodaj_pitanje(){

        $poruka = $_POST["poruka"];
        $naslov = $_POST["naslov"];
        $datum = date("Y-m-d H:i:s");
        
        
        $db = \Config\Database::connect();
        $sql = "INSERT INTO forma VALUES(NULL,'".$naslov."','".$poruka."','".$datum."')";
        $db->query($sql);
            
        $sql = "SELECT idForma FROM forma ORDER BY idForma DESC LIMIT 1";
        $query = $db->query($sql);
        $result = $query->getResult();
        $ins_id = $result[0]->idForma;

        $username = $_SESSION["korisnik"][0]->username;

        $sql = "INSERT INTO pitanje VALUES(".$ins_id.",'".$username."',0)";
        $db->query($sql);
        return redirect()->to("http://localhost:8080/admin");
        
    }*/
/**
 * Funkcija pitanaj() postavlja sva odgovarajuca pitanja na uvid adminu zajedno sa postojecim odgovorima
 * @return void
 * 
 */
    public function pitanja(){
        /**
         * var array $pitanja
         * var string $odgovori
         * var int $ids
         */
        echo view("head");
        echo view("navigacija");
        if(isset($_SESSION["korisnik"])){
            if($_SESSION["korisnik"][0]->Tip==2){
                echo view("template/menu");
               
            }else if($_SESSION["korisnik"][0]->Tip==1){
                echo view("template/menu2");
               
            }
        }
        $pitanja = $this->vrati_pitanja();
        $odgovori = $this->vrati_odgovore($pitanja);
        $ids = $this->vrati_id_odgovora($pitanja);
        echo view("Pitanja.php",["pitanja"=>$pitanja,"odgovori"=>$odgovori,"ids"=>$ids]);
        echo view("footer.php");
    }
/**funkcija vrati_lekove() vraca sve lekove iz baze
 * @return array
 */
    public function vrati_lekove(){
        $db = \Config\Database::connect();
        $query = $db->query("SELECT * FROM lek");
        $result = $query->getResult();
        return $result;
    }
/**Funkcija lek() prikazuje sve lekove 
 * @return void
 * 
 */
    public function lek(){
        echo view("head");
        echo view("navigacija");

        if(isset($_SESSION["korisnik"])){
            if($_SESSION["korisnik"][0]->Tip==2){
                echo view("template/menu");
               
            }else if($_SESSION["korisnik"][0]->Tip==1){
                echo view("template/menu2");
               
            }
        }
        $result = $this->vrati_lekove();

        $db = \Config\Database::connect();

        
      
        echo view("admin_lek",["podaci"=>$result]);
        echo view("footer");
    }
/**
 * Funkcija izmeni lek menja naziv i cenu leku o takodje sluzi za brisanje lekova
 * @return void
 */
    public function izmeni_lek(){
        if(isset($_POST["izmeni"])){
/**
 * var string $operacija
 * var int $id
 */
            $operacija = $_POST["izmeni"];
            $id = $_POST["id"];

            $db = \Config\Database::connect();

            if($operacija=="izmeni"){
                /**
                 * var string $naziv
                 * var int $cena
                 * var string $sql
                 */
                $naziv = $_POST["naziv"];
                $cena = $_POST["cena"];
                $sql = "UPDATE lek SET naziv='".$naziv."',cena=".$cena." WHERE idLek=".$id;
            }
            if($operacija=="obrisi"){
                $sql = "DELETE FROM lek WHERE idLek=".$id;
            }
            $query = $db->query("SET FOREIGN_KEY_CHECKS=0;");
            $db->query($sql);
            $this->lek();
        }
    }
/** 
 * Funkcija za dodavanje leka u bazu podataka
 * @return void
*/
    public function operacije(){
       
        /**
         * var strig $naziv
         * var string $cena
         * var string $opis
         * var string $uputstvo
         */

        if(isset($_POST["dodaj_lek"])){
            $naziv = $_POST["naziv"];
            $cena = $_POST["cena"];
            $opis = $_POST["opis"];
            $uputstvo = $_POST["uputstvo"];
            /**
             * var string $sql
             */

            $db = \Config\Database::connect();
            $sql = "INSERT INTO lek VALUES (NULL,".$db->escape($naziv).", ".$db->escape($opis).", ".$db->escape($uputstvo).", ".$db->escape($cena).")";
            $db->query($sql);
            if($db->affectedRows()>0){
                $odg = "lek dodat";
            }else{
                $odg = "greska";
            }
            /*
            echo view("head.php");
            echo view("navigacija.php");
            echo view("template/menu");
            $result = $this->vrati_lekove();
            echo view("admin_lek.php",["podaci"=>$result,"odgovor"=>$odg]);
            echo view("footer.php");*/

            return redirect()->to('/admin'); 
        }
    }

    /*public function admin(){
        echo view("head");
        echo view("navigacija");
        echo view("template/menu");
        echo view("footer");
    }*/

   /* public function nalog(){
        echo view("head.php");
        echo view("navigacija.php");
        echo view("template/menu");

        $db = \Config\Database::connect();
        $query = $db->query("SELECT * FROM korisnik");
        $result = $query->getResult();

        


        echo view("admin_korisnik.php",["podaci"=>$result]);

        echo view("footer.php");
    }*/





 //Dimitrije
 protected function prikaz($page, $data)
    {
       
        $data["controller"] = "Admin";
        echo view("template/header", $data);
        echo view("navigacija");
        
        if(isset($_SESSION["korisnik"])){
            if($_SESSION["korisnik"][0]->Tip==2){
                echo view("template/menu");
                echo view("pages/{$page}", $data);
            }else if($_SESSION["korisnik"][0]->Tip==1){
                echo view("template/menu2");
                echo view("pages/{$page}", $data);
            }
        }
        /*drugi argument salje podatke stranici u obliku niza koji se posle
        raspakuje*/
        
        echo view("template/footer");
    }
    public function getFromBase($type)
    {
        $korMod = new AdminModel();
        return $korisnici = $korMod->{$type}();
    }
    
    public function zahtevi()
    {
        $this->prikaz("zahtevi", ["korisnici" => $this->getFromBase("zahtevi")]);
    }
    
    public function odobreni()
    {
        $this->prikaz("odobreni", ["korisnici" => $this->getFromBase("odobreni")]);
    }
    
    public function odobri()
    {
        if(!$this->validate(["username" => "required"]))
        {
            return $this->prikaz("zahtevi", ["odgovor" => $this->validator->listErrors()]);
        }
        $user = $this->request->getVar("username");
        $korMod = new AdminModel();
        $korMod->izmeni($user, 1);
        return redirect()->to(site_url("Admin/zahtevi"));
    }
    
    public function suspendujObrisi()
    {
        if(!$this->validate(["username" => "required"]))
        {
            return $this->prikaz("odobreni", ["korisnici" => $this->getFromBase("odobreni"), 
                "odgovor" => $this->validator->listErrors()]);
        }
        
        if($this->request->getVar("suspenduj") == null)
        {   
            if($this->request->getVar("obrisi") == null)
            {
                return $this->prikaz("odobreni", ["korisnici" => $this->getFromBase("odobreni"), 
                    "odgovor" => "GRESKA: Niste izabrali ni jedno dugme"]);
            }
            else
            {
                $db = \Config\Database::connect();
                $query = $db->query("SET FOREIGN_KEY_CHECKS=0;");
                $user = $this->request->getVar("username");
                
               $query = $db->query("DELETE FROM korpa WHERE username='".$user."'");
                $korMod = new AdminModel();
                $korMod->obrisi($user);
                return redirect()->to(site_url("Admin/odobreni"));
            }
        }
        else 
        {
            $user = $this->request->getVar("username");
            $korMod = new AdminModel();
            $korMod->izmeni($user, 2);
            return redirect()->to(site_url("Admin/odobreni"));
        }
    }
    
    public function ukloniSuspenziju()
    {
        if(!$this->validate(["username" => "required"]))
        {
            return $this->prikaz("zahtevi", ["korisnici" => $this->getFromBase("odobreni"), 
                "odgovor" => $this->validator->listErrors($template)]);
        }
        $user = $this->request->getVar("username");
        $korMod = new AdminModel();
        $korMod->izmeni($user, 1);
        return redirect()->to(site_url("Admin/odobreni"));
    }
    
    public function index()
    {
        $this->odobreni();
    }
}
?>