<?php


namespace App\Controllers;
   
use CodeIgniter\Controller;

/**
 * Reklamacije - klasa za belezenje reklamacija u bazi podataka
 * Isidora Milosevic2018/0660
 * @version 1.0
 */


class Reklamacije extends BaseController{
    /**
     * @return void
     * Ucitavanje pocetne strane klase Reklamacije
     */
    
    public function index(){
        
        echo view("head");
        echo view("navigacija");
        echo view("reklamacije");
        echo view("footer");
        
    }
    /**
     * Funkcija za popunjavanje baze-> popunjava tabelu Reklamacije
     * Pomocu dohvatanja podataka iz forme i ubacivanaj koristeci sql INSERT
     * @return string
     */

    public function slanje(){
            /**
             * var string $poruka Poruka
             * var string $naslov Naslov
             * var datetime $datum  DatumVreme
             */
            $poruka = $_POST["poruka"];
            $naslov = $_POST["naslov"];
            $datum = date("Y-m-d H:i:s");
            
            /**
             * object $db  veza sa bazom podataka
             */
            $db = \Config\Database::connect();
            /**
             * var string $sql
             */
            $sql = "INSERT INTO forma VALUES(NULL,'".$naslov."','".$poruka."','".$datum."')";
            $db->query($sql);
            /**
             * var array $result
             * var int $ins_id
             */
            $sql = "SELECT idForma FROM forma ORDER BY idForma DESC LIMIT 1";
            $query = $db->query($sql);
            $result = $query->getResult();
            $ins_id = $result[0]->idForma;
            /**
             * var string $username
             */
    
            $username = $_SESSION["korisnik"][0]->username;
    
            $sql = "INSERT INTO reklamacija VALUES(".$ins_id.",'".$username."')";
            $db->query($sql);
            return redirect()->to('http://localhost:8080/reklamacije');
            

            

    }
   

}


?>