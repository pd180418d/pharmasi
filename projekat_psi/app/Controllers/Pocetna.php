<?php 

namespace App\Controllers;
   
use CodeIgniter\Controller;
/**
 * Klasa - Pocetna sluzi za prikazivanje svih lekova iz baze,za prikazivanje opcija filtera i takodje za informacije o svakom pojedinacnom leku
 * Nemanja Ciric 0548/18
 * @version 1.0
 */

class Pocetna extends BaseController{
    /**
     * Funkcija dodaj() prestavlja dodavanje lekova u korpu
     * @return void
     */

public function dodaj(){
    //echo view("head.php");
    //echo view("navigacija.php");

    /**
     * var int $kolicina
     * var int $id_lek
     * var string $korime_korisnika username
     * $db predstavlja inicijalizaciju veze sa bazom
     */
   

    $kolicina = 1;
    $id_lek = $_POST["korpa"];
    $korime_korisnika = $_SESSION["korisnik"][0]->username;
    $db = \Config\Database::connect();
    /**
     * var string $sql
     * $result predstavlja asocijativni niz dobijen izvrsavanjem sql upita
     */

    $sql = "SELECT * FROM korpa WHERE username='".$korime_korisnika."' AND idLek=".$id_lek;
    $query = $db->query($sql);
    $result = $query->getResult();

    /**
     * Ako je vracen 1 rezultat pravimo upit za povecanje kolicine tog leka
     * Ako nije vracen nijedan rezultat pravimo upit za dodavanje nove istance tog leka u bazu
     */

    if(count($result)>0){
        $sql = "UPDATE korpa SET kolicina=kolicina+1 WHERE username='".$korime_korisnika."' AND idLek=".$id_lek;
    }else{
        $sql = "INSERT INTO korpa VALUES('".$korime_korisnika."',".$id_lek.",".$kolicina.")";
    }
    /**
     * Izvrsava se upit
     */
    $db->query($sql);
    if($db->affectedRows()>0){
        $query = $db->query("SELECT * FROM Lek");
        $result = $query->getResult();
    }else{
        $query = $db->query("SELECT * FROM Lek");
        $result = $query->getResult();
    }

    return redirect()->to('/Pocetna'); 


    //echo view("footer.php");
}
/**
 *  Funkcija recenzija() sakuplja utiske korisnika na odgovarajuci lek i belezi ih u bazu
 * @return void
 */
    public function recenzija(){
        
        if(isset($_POST["recenzija"])){
            /**
             * var string $poruka 
             * var string $naslov
             * var string $ocena
             * var int $id
             * var datetime $datum
             */
            $poruka = $_POST["poruka"];
            $naslov = $_POST["naslov"];
            $ocena = $_POST["ocena"];
            $id = $_POST["recenzija"];
            $datum = date("Y-m-d H:i:s");
            /**
             * Upisujemo prethodne informacije u tabelu forma
             */
            
            $db = \Config\Database::connect();
            $sql = "INSERT INTO forma VALUES(NULL,'".$naslov."','".$poruka."','".$datum."')";
            $db->query($sql);
                
            /**
             * Selektujemo poslednju dodatu formu 
             * var string $username
             * var int $ins_id
             */
            $sql = "SELECT idForma FROM forma ORDER BY idForma DESC LIMIT 1";
            $query = $db->query($sql);
            $result = $query->getResult();
            $ins_id = $result[0]->idForma;
    
            $username = $_SESSION["korisnik"][0]->username;
    /**
     * Upisujemo date podatke u tabelu recenzija
     */
            $sql = "INSERT INTO recenzija VALUES(".$ins_id.",".$id.",'".$username."',".$ocena.")";
            $db->query($sql);
            return redirect()->to('http://localhost:8080/pocetna/lek/'.$id);
            

            

        }
    }
/**
 * Funkcija za prikazivanje posebnih informacija za svaki lek   
 * @param int $id default 1
 * @return void
 */
    public function lek($id=1){
       
        echo view("head");
        echo view("navigacija");
        $db = \Config\Database::connect();
        $query = $db->query("SELECT * FROM Lek WHERE idLek=".$id);
        $result = $query->getResult();

        $query = $db->query("SELECT AVG(ocena) AS prosek FROM recenzija WHERE idLek=".$id);
        $r = $query->getResult();

        $query = $db->query("SELECT forma.Naslov,forma.Poruka,forma.DatumVreme,recenzija.username,recenzija.Ocena FROM forma,recenzija WHERE recenzija.idForma=forma.idForma AND recenzija.idLek=".$id);
        $r2 = $query->getResult();

        echo view("lek.php",["podaci"=>$result,"prosek"=>$r[0]->prosek,"recenzije"=>$r2]);

        echo view("footer");
    }
    /**
     * Funckija index prikazuje pocetnu stranicu
     * @return void
     * @param string $msg
     */

    public function index($msg=""){
       
        echo view("head");

        if(isset($_SESSION["korisnik"])){
            $db = \Config\Database::connect();
            $query = $db->query("SELECT SUM(kolicina) AS broj FROM korpa WHERE username='".$_SESSION["korisnik"][0]->username."'");
            $result = $query->getResult();
            $broj = $result[0]->broj;

            echo view("navigacija",["korpa_broj"=>$broj]);
        }else{
            echo view("navigacija");
        }

        $db = \Config\Database::connect();
        

        if(isset($_POST["sort"])){
            if($_POST["sort"]=="sort_asc"){
                $query = $db->query("SELECT * FROM Lek ORDER BY Cena ASC");
            }
            if($_POST["sort"]=="sort_desc"){
                $query = $db->query("SELECT * FROM Lek ORDER BY Cena DESC");
            }
        }else{
            $query = $db->query("SELECT * FROM Lek");
        }

        $result = $query->getResult();
        echo view("lekovi",["podaci"=>$result,$odgovor=$msg]);

        echo view("footer");
    }

}

?>