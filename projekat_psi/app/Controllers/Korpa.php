<?php 

namespace App\Controllers;
   
use CodeIgniter\Controller;

/**
 * Klasa - Pocetna sluzi za prikazivanje svih lekova iz baze,za prikazivanje opcija filtera i takodje za informacije o svakom pojedinacnom leku
 * Nemanja Ciric 0548/18
 * @version 1.0
 */

class Korpa extends BaseController{
/**
 * Funkcija index je defaultni poziv prilikom ulaska na stranicu Korpa
 * @return void
 */
    public function index(){
        
        echo view("head.php");
        echo view("navigacija.php");
/**
 * Dohvata se trenutni korisnik u sesiji
 * var array $k
 */
        if(isset($_SESSION['korisnik'])){
            $k = $_SESSION["korisnik"][0];

         

        $db = \Config\Database::connect();
        $query = $db->query("SELECT korpa.kolicina AS kolicina, lek.Naziv AS ime FROM korisnik,korpa,lek WHERE korisnik.username=korpa.username AND korpa.idLek=lek.idLek AND korisnik.username='".$k->username."'");
        $result = $query->getResult();
        /**
         * Provera da li postoje stvari u korpi
         */
        if(count($result)>0){
            echo view("korpa.php",["podaci"=>$result]);
           
        }else{
            echo view("korpa.php",["podaci"=>[]]);
        }

        echo view("footer.php");
        }else{
           
            $odg=["odgovor"=>"Niste ulogovani"];
            echo view("uloguj.php",$odg);
        }

        
    }
    
    public function kraj(){
        

            if(isset($_SESSION['korisnik'])){
                $db = \Config\Database::connect();
               
             
                $query = $db->query("DELETE FROM korpa WHERE username='".$_SESSION["korisnik"][0]->username."'");
                return redirect()->to('http://localhost:8080/Korpa');
            }
            
           
    

    }

}


    

?>