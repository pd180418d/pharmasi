<?php 

namespace App\Controllers;
   
use CodeIgniter\Controller;

/**
 * Logovanje - klasa za login vec postojecih korisnika
 * Anja Mladenovic 2018/0478
 * @version 1.0
 */
class Logovanje extends BaseController{
    /**
     * Funkcija index prikazuje podrazumevanu stranicu za logovanje
     */

    public function index(){
       /**
        * var array $k
        */
        if(isset($_SESSION['korisnik'])){
            $k = $_SESSION["korisnik"];
            echo view("head.php");
            echo view("navigacija.php");
            echo view("profil.php",["podaci"=>$k]);
        }else{
            echo view("head.php");
            echo view("navigacija.php");
            echo view("uloguj.php");
            echo view("footer.php");
        }
        
    }
    /**
     * Funkcija login sluzi da se pokupi korisicno ime i lozinka,proveri da li postoje u bazi
     * I Uloguje/odbije logovanje korisnika
     */

     /*
     salt = 13524
     $l = "password"

     $l2 = "password"+"13524" => "password13524"
     */ 

    public function login(){
        /**
         * var string $korime
         * var string $lozinka
         */
        
        echo view("head.php");
        echo view("navigacija.php");

        $korime = $_POST["korime"];
        $lozinka = $_POST["lozinka"];

        $db = \Config\Database::connect();
        $query = $db->query("SELECT * FROM korisnik WHERE username='".$korime."' AND password='".$lozinka."'");
        $result = $query->getResult();
        
        
        /**
         * Provera da li postoji korisnik
         */
        if(count($result)>0){
            $_SESSION["korisnik"] = $result;
            $odg=["odgovor"=>"ulogovani ste"];
            echo view("uloguj.php",$odg);
        }else{
            $odg=["odgovor"=>"korisnik ne postoji, registrujte se"];
            echo view("Registruj.php",$odg);
        }
        
        
        echo view("footer.php");
    }

}

?>