//Nemanja Ciric


var prikazkorisnici = document.getElementById("prikazkorisnici");
var korisnici = document.getElementById("korisnici");

var prikazlekovi = document.getElementById("prikazlekovi");
var lekovi = document.getElementById("lekovi");
var mesto = document.getElementById("mesto");
var uklanjanje_suspenzije = 0;


var postaviPitanje = document.getElementById("postavi_pitanje")

if(postaviPitanje !=null){
    var ulogovan = JSON.parse(localStorage.getItem("ulogovan"));
    if(ulogovan==null){
        document.location.href="logovanje.html";
    }

    postaviPitanje.addEventListener("click",()=>{
        var pitanje = document.getElementById("pitanje").value;

        if(pitanje == ""){
            alert("Niste postavili pitanje");
        }else{
            alert("Pitanje je poslato");
        }
    })
}

var posalji_poruku = document.getElementById("posalji_poruku");

if(posalji_poruku!=null){
    posalji_poruku.addEventListener("click",()=>{
        var pitanje = document.getElementById("poruka").value;

        if(pitanje == ""){
            alert("Forma za poruku je prazna");
        }else{
            alert("Pitanje je prosledjeno administraciji");
        }
    })
}

var reklamacija = document.getElementById("prilozite_reklamaciju");

if(reklamacija!=null){
    var ulogovan = JSON.parse(localStorage.getItem("ulogovan"));
    if(ulogovan==null){
        document.location.href="logovanje.html";
    }
}



if(prikazkorisnici!=null){
    var ulogovan = JSON.parse(localStorage.getItem("ulogovan"));
    if(ulogovan==null){
        document.location.href="logovanje.html";
    }
}



if(prikazkorisnici!=null && korisnici!=null){
    
    korisnici.style.display = "none";
    for(var i = 0; i<4;i++){
        var element = document.getElementById((i + 1) + "" + (i + 1));
        element.style.display = "none";
    }
   prikazkorisnici.addEventListener("click",()=>{
        if(lekovi.style.display =="block" || pitanja.style.display == "block" || zahtevi.style.display == "block"){
            lekovi.style.display = "none";
            pitanja.style.display = "none";
            zahtevi.style.display = "none";
            korisnici.style.display = "block";
        }else{
            if(korisnici.style.display=="none"){
                korisnici.style.display="block";
                   
                
            }else{
                korisnici.style.display="none";
            }
        }

       
    });
}

if(prikazlekovi!=null && lekovi!=null){
    lekovi.style.display = "none";
    prikazlekovi.addEventListener("click",()=>{

        if(korisnici.style.display =="block" || pitanja.style.display == "block" || zahtevi.style.display == "block"){
            korisnici.style.display ="none";
            pitanja.style.display = "none";
            zahtevi.style.display = "none";
            lekovi.style.display = "block";
        }else{
            if(lekovi.style.display=="none"){
                lekovi.style.display="block";
            }else{
                lekovi.style.display="none";
            }
        }


        
    });
}

var prikazpitanja = document.getElementById("prikazpitanja");
var pitanja = document.getElementById("pitanja");

if(prikazpitanja!=null && pitanja!=null){
    pitanja.style.display="none";
    prikazpitanja.addEventListener("click",()=>{

        if(korisnici.style.display =="block" || lekovi.style.display == "block" || zahtevi.style.display == "block"){
            korisnici.style.display = "none";
            lekovi.style.display = "none";
            zahtevi.style.display = "none";
            pitanja.style.display = "block";
        }else{
            if(pitanja.style.display=="none"){
                pitanja.style.display="block";
            }else{
                pitanja.style.display="none";
            }
        }

       
    });
}

var prikazzahteva = document.getElementById("prikazzahteva");
var zahtevi = document.getElementById("zahtevi");

if(prikazzahteva!=null && zahtevi!=null){
    zahtevi.style.display="none";
    prikazzahteva.addEventListener("click",()=>{
        if(korisnici.style.display =="block" || lekovi.style.display == "block" || pitanja.style.display == "block"){
            korisnici.style.display = "none";
            lekovi.style.display = "none";
            pitanja.style.display = "none";
            zahtevi.style.display = "block";
        }else{
            if(zahtevi.style.display=="none"){
                zahtevi.style.display="block";
            }else{
                zahtevi.style.display="none";
            }
        }
       
    });
}

var lek = function(ime,cena){
    this.ime=ime;
    this.cena=cena;
}

var korpa = document.getElementsByClassName("korpa");
for(var i=0;i<korpa.length;i++){
    korpa[i].addEventListener("click",(e)=>{
        //document.location.href="korpa.html";
        //console.log(e.target.getAttribute("ime")+" "+e.target.getAttribute("cena"));
        var korpa_niz = JSON.parse(localStorage.getItem("korpa"));
        if(korpa_niz==null){
            korpa_niz=[];
        }
        korpa_niz.push(new lek(e.target.getAttribute("ime"),e.target.getAttribute("cena")));
        localStorage.setItem("korpa",JSON.stringify(korpa_niz));
    });
}

//logovanje

var status = false;
var ulogovan;

var osoba = function(ime,prezime,korime,lozinka,email,status="korisnik"){
    this.ime=ime;
    this.prezime=prezime;
    this.korime=korime;
    this.lozinka=lozinka;
    this.email=email;
    this.status=status;
}

var info = function(poruka){
    document.getElementById("odg").innerHTML=poruka;
}

var registruj = document.getElementById("registruj");
if(registruj!=null){
    var ulogovan = JSON.parse(localStorage.getItem("ulogovan"));
    if(ulogovan!=null){
        document.location.href="profil.html";
    }
    registruj.addEventListener("click",(e)=>{
        e.preventDefault();
        //ime,prezime,korime,lozinka,potvrda,email
        var ime = document.getElementById("ime").value;
        var prezime = document.getElementById("prezime").value;
        var korime = document.getElementById("korime").value;
        var lozinka = document.getElementById("lozinka").value;
        var potvrda = document.getElementById("potvrda").value;
        var email = document.getElementById("email").value;

        if(ime.length<3){
            info("Unesite Ime");
            window.scrollTo(0,0);
            return;
        }

        if(prezime.length<3){
            info("Unesite Prezime");
            window.scrollTo(0,0);
            return;
        }

        if(korime.length<3){
            info("Unesite Korisnicko ime");
            window.scrollTo(0,0);
            return;
        }

        if(lozinka.length<3){
            info("Unesite Lozinku");
            window.scrollTo(0,0);
            return;
        }

        if(potvrda.length<3){
            info("Unesite Potvrdu lozinke");
            window.scrollTo(0,0);
            return;
        }

        if(lozinka!=potvrda){
            info("Lozinka i Potvrda se ne podudaraju");
            window.scrollTo(0,0);
            return;
        }

        if(email.length<3){
            info("Unesite Email");
            window.scrollTo(0,0);
            return;
        }

        if(!email.includes("@")){
            info("Neispravan format Email adrese");
            window.scrollTo(0,0);
            return;
        }

        var niz = JSON.parse(localStorage.getItem("korisnici"));
        if(niz==null){
            niz = [];
        }
        var o = new osoba(ime,prezime,korime,lozinka,email);
        niz.push(o);
        localStorage.setItem("korisnici",JSON.stringify(niz));
        info("korisnik registrovan");
       
    });
}

var logovanje = document.getElementById("logovanje");
if(logovanje!=null){
    var ulogovan = JSON.parse(localStorage.getItem("ulogovan"));
    if(ulogovan!=null){
        document.location.href="profil.html";
    }
    logovanje.addEventListener("click",(e)=>{
        e.preventDefault();
        var korime = document.getElementById("korime").value;
        var lozinka = document.getElementById("lozinka").value;

        var niz = JSON.parse(localStorage.getItem("korisnici"));
        if(niz==null){
            niz = [];
        }

        for(var i=0;i<niz.length;i++){
            if(niz[i].korime==korime && niz[i].lozinka==lozinka){
                status=true;
                ulogovan = new osoba(niz[i].ime,niz[i].prezime,niz[i].korime,niz[i].lozinka,niz[i].email);
                localStorage.setItem("ulogovan",JSON.stringify(ulogovan));
            }
        }

        if(ulogovan!=null){
            alert("ulogovani ste");
            document.location.href = "profil.html" // izmena za logovanje
        }else{
            alert("greska");
        }
    });
}

var promeni = document.getElementById("promeni");
if(promeni!=null){
    var ulogovan = JSON.parse(localStorage.getItem("ulogovan"));
    if(ulogovan!=null){
        //korisnik je ulogovan
        var st = document.getElementById("sta");
        var ime = document.getElementById("ime");
        var prezime = document.getElementById("prezime");
        var korime = document.getElementById("korime");
        var lozinka = document.getElementById("lozinka");
        var email = document.getElementById("email");

        st.value = ulogovan.status;
        ime.value = ulogovan.ime;
        prezime.value = ulogovan.prezime;
        korime.value = ulogovan.korime;
        lozinka.value = ulogovan.lozinka;
        email.value = ulogovan.email;
    }else{
        //korisnik nije ulogovan
        document.location.href="logovanje.html";
    }

    promeni.addEventListener("click",(e)=>{
        e.preventDefault();
        var ime = document.getElementById("ime").value;
        var prezime = document.getElementById("prezime").value;
        var korime = document.getElementById("korime").value;
        var lozinka = document.getElementById("lozinka").value;
        var potvrda = document.getElementById("potvrda").value;
        var email = document.getElementById("email").value;

        if(potvrda!=lozinka){
            info("lozinka i potvrda se ne podudaraju");
        }

        var o = new osoba(ime,prezime,korime,lozinka,email);
        localStorage.setItem("ulogovan",JSON.stringify(o));
        var niz = JSON.parse(localStorage.getItem("korisnici"));
        for(var i=0;i<niz.length;i++){
            if(niz[i].korime==korime){
                niz[i]=o;
            }
        }
        localStorage.setItem("korisnici",JSON.stringify(niz));
        document.location.reload();

    });
}

var izloguj = document.getElementById("izloguj");
if(izloguj!=null){
    izloguj.addEventListener("click",()=>{
        localStorage.removeItem("ulogovan");
        localStorage.removeItem("korpa");
        document.location.reload();
    });
}

var kupljeno = document.getElementById("kupljeno");
if(kupljeno!=null){
    var s = 0;
    var niz2 = JSON.parse(localStorage.getItem("korpa"));
    if (niz2!=null){
        for(var i=0;i<niz2.length;i++){
            s+=Number(niz2[i].cena);
            kupljeno.innerHTML+=niz2[i].ime+" "+niz2[i].cena+"<br>";
        }
        kupljeno.innerHTML+="ukupno: "+s;
    }else{
        var ulogovan = JSON.parse(localStorage.getItem("ulogovan"));
        if(ulogovan!=null){
            kupljeno.innerHTML+="Vasa korpa je prazna"
        }else{
            kupljeno.innerHTML+="Morate se ulogovati"
        }
        
    }
    
}


// zavrsi kupovinu
var zavrsi=document.getElementById("zavrsi_kupovinu");

if(zavrsi!=null){
    zavrsi.addEventListener("click",(e)=>{
        e.preventDefault();
        localStorage.removeItem("korpa");
        document.location.href="index.html";
    });
}







// sortiranje

class apoteka_lek {
    constructor(ime, cena, slika, opis) {
        this.ime = ime;
        this.cena = cena;
       
        this.slika = slika;
        this.opis = opis;

    }
}

var apoteka = localStorage.getItem("apoteka");

if(apoteka==null){
    var lista={
        apoteka:[]
    }
    var l1 = new apoteka_lek("Brufen",100,1,"lek"); //dodati i opis 
    var l2 = new apoteka_lek("Dimigal",200,2,"lek");
    var l3 = new apoteka_lek("Paracetamol",300,3,"lek");
    var l4 = new apoteka_lek("Probiotic",120,4,"lek");
    var l5 = new apoteka_lek("Fervex",50,5,"lek");
    var l6 = new apoteka_lek("TantumVerde",60,6,"lek")
    var l7 = new apoteka_lek("Coldrex",700,7,"lek");
    var l8 = new apoteka_lek("Hiper hama gel",800,8,"lek");
    var l9 = new apoteka_lek("Konjska mast",900,9,"lek")

    lista.apoteka.push(l1);
    lista.apoteka.push(l2);
    lista.apoteka.push(l3);
    lista.apoteka.push(l4);
    lista.apoteka.push(l5);
    lista.apoteka.push(l6);
    lista.apoteka.push(l7);
    lista.apoteka.push(l8);
    lista.apoteka.push(l9);

    localStorage.setItem("apoteka",JSON.stringify(lista));
    if(mesto!=null){
        ucitaj_lekove(JSON.parse(localStorage.getItem("apoteka")),mesto);
    }
   
}else{
    if(mesto!=null){
        ucitaj_lekove(JSON.parse(localStorage.getItem("apoteka")),mesto);
    }
}


var checkbox_ASC=document.getElementById("ASC");
var checkbox_DESC=document.getElementById("DESC");




function ucitaj_lekove(lekovi,mesto){

    if(mesto!=null){
        mesto.innerHTML="";
    var brojac = 0;
    for(var i=0;i<lekovi.apoteka.length;i++){
        brojac++;
        var col = document.createElement("div");
        col.classList.add("col-12");
        col.classList.add("col-md-3");
        col.classList.add("offset-md-0.5")
        col.classList.add("mt-5");
        col.classList.add("text-center");
        
        var card_group = document.createElement("div");
        card_group.classList.add("card-group");

        var c2 = document.createElement("div");
        c2.classList.add("card");
        c2.classList.add("text-center");


        
      
        var img = document.createElement("img");
        img.classList.add("card-img-top");
        img.src="slike/"+lekovi.apoteka[i].slika+".jpg";

        c2.appendChild(img);

        var body = document.createElement("div");
        body.setAttribute("class","card-body");

        var h5 = document.createElement("h5");
        h5.setAttribute("class","card-title");
        h5.innerHTML=lekovi.apoteka[i].ime + " ";
        body.appendChild(h5);

        var p = document.createElement("p");
        p.setAttribute("class","card-text");
        p.innerHTML="opis: " + lekovi.apoteka[i].opis;
        body.appendChild(p);

        var p = document.createElement("p");
        p.setAttribute("class","card-text");

        var small = document.createElement("small");
        small.setAttribute("class","text-muted");
        small.innerHTML="cena: " + lekovi.apoteka[i].cena;

        p.appendChild(small);

        var razmak = document.createElement("br");

        p.appendChild(razmak)

        var ocena = document.createElement("span");

        ocena.innerHTML = "Ocenite: "

        p.appendChild(ocena);

        var range_info = document.createElement("span");
            range_info.innerHTML=3;
            var range = document.createElement("input");
            range.type="range";
            range.min=0;
            range.max=5;
            range.value=3;
            p.appendChild(range);

            p.appendChild(range_info);
            range.info=range_info;
            range.addEventListener("click",(e)=>{
                e.target.info.innerHTML=e.target.value;
            });

  

        body.appendChild(p);


        c2.appendChild(body);
        
        card_group.appendChild(c2);

        col.appendChild(card_group);
        mesto.appendChild(col);
        var dugme = document.createElement("button");

        dugme.classList.add("btn");
        dugme.classList.add("btn-primary");
        dugme.classList.add("korpa");
        dugme.setAttribute("ime",lekovi.apoteka[i].ime);
        dugme.setAttribute("cena",lekovi.apoteka[i].cena);
        dugme.innerHTML = "Dodaj u korpu";
        dugme.setAttribute("id","brojac"+brojac);

        body.appendChild(dugme);

        dugme.addEventListener("click",(e)=>{
            //document.location.href="korpa.html";
            //console.log(e.target.getAttribute("ime")+" "+e.target.getAttribute("cena"));
            var korpa_niz = JSON.parse(localStorage.getItem("korpa"));
            var ulogovan = JSON.parse(localStorage.getItem("ulogovan"));
            if(korpa_niz==null){
                korpa_niz=[];
            }
            if(ulogovan!=null){
                korpa_niz.push(new lek(e.target.getAttribute("ime"),e.target.getAttribute("cena")));
                localStorage.setItem("korpa",JSON.stringify(korpa_niz));
                update_korpa();
            }else{
                alert("Morate biti ulogovani da dodate u korpu");
            }
            
        });
    }
    }
    
    
    
}



function ucitaj_listu_lekova(lekovi){
    
}


var update_korpa = function(){
    var korpalink = document.getElementById("korpalink");
    if(korpalink!=null){
        var lekovi = JSON.parse(localStorage.getItem("korpa"));
        if(lekovi!=null){
            korpalink.innerHTML="korpa("+lekovi.length+")";
        }
    }
}
update_korpa();

if (checkbox_ASC!=null){
    
    checkbox_ASC.addEventListener("click",()=>{
       

        var lekovi = JSON.parse(localStorage.getItem("apoteka"));
        for(var i=0;i<lekovi.apoteka.length;i++){
            for(var j=0;j<lekovi.apoteka.length-1;j++){
                if(lekovi.apoteka[i].cena<lekovi.apoteka[j].cena){
                    var pom = lekovi.apoteka[i];
                    lekovi.apoteka[i]=lekovi.apoteka[j];
                    lekovi.apoteka[j]=pom;
                    
                }
            }
        }

        ucitaj_lekove(lekovi,mesto); // ?
    });
   
}

if(checkbox_DESC!=null){
    checkbox_DESC.addEventListener("click",()=>{
        
        

        var lekovi = JSON.parse(localStorage.getItem("apoteka"));
        for(var i=0;i<lekovi.apoteka.length;i++){
            for(var j=0;j<lekovi.apoteka.length-1;j++){
                if(lekovi.apoteka[i].cena>lekovi.apoteka[j].cena){
                    var pom = lekovi.apoteka[i];
                    lekovi.apoteka[i]=lekovi.apoteka[j];
                    lekovi.apoteka[j]=pom;
                    
                }
            }
        }

        ucitaj_lekove(lekovi,mesto); // ?
  


    });
}


function sakrij(id){
    var dugme = document.getElementById(id);
    dugme.style.display = "none";

 

    var nalog = document.getElementById(id + "" + id);
    
    nalog.style.display = "block";

    alert("Odobrili ste korisnicki nalog");
}

function obrisi_nalog(id){
    var nalog = document.getElementById(id);
    nalog.style.display="none";

    alert("Obrisali ste korisnicki nalog");
}







function suspenduj_nalog(ime,dugme_potvrda,dugme_brisanje,dugme_suspenduj,kor_ime){
     var nalog = document.getElementsByClassName(ime);
     var vrednost = localStorage.getItem(ime);
     var postavljen = false;

     if(vrednost == null){
         vrednost = 0;
     }

     if(vrednost == 0){

       

        for(var i = 0; i<nalog.length;i++){
            if(i == 3){
               nalog[i].innerHTML = "SUSPENDOVAN NALOG";
            }else{
                nalog[i].style.color = "red";
            }
            
            nalog[i].style.backgroundColor="orange";
        }
   
        var dugme1 = document.getElementById(dugme_potvrda);
        if(dugme1.style.display != "none"){
            postavljen = true;
        }
        dugme1.style.display = "none";
   
   
        var dugme2 = document.getElementById(dugme_brisanje);
        dugme2.style.display = "none";
   
   
        var dugme3 = document.getElementById(dugme_suspenduj);
   
        dugme3.innerHTML = "Ukloni suspenziju";

        uklanjanje_suspenzije = 1;
   
        localStorage.setItem(ime,uklanjanje_suspenzije);
        localStorage.setItem("dugme"+ime,postavljen);
        alert("Suspendovali ste nalog");

     }else{

        for(var i = 0; i<nalog.length;i++){
            if(i == 3){
               nalog[i].innerHTML = "Korisnicko ime: <b>" + kor_ime + "</b>";
            }else{
                nalog[i].style.color = "black";
            }
            
            nalog[i].style.backgroundColor="white";
        }

        var postavljenost = localStorage.getItem("dugme"+ime);


        if(postavljenost == "true"){
            var dugme1 = document.getElementById(dugme_potvrda);
            dugme1.style.display = "inline-block";
        }
       
   
   
        var dugme2 = document.getElementById(dugme_brisanje);
        dugme2.style.display = "inline-block";
   
   
        var dugme3 = document.getElementById(dugme_suspenduj);
   
        dugme3.innerHTML = "Suspenduj nalog";

        uklanjanje_suspenzije = 0;
        localStorage.setItem(ime,uklanjanje_suspenzije);
   
        alert("Otklonili ste suspenziju");

     }



    
     
}